
<!-- README.md is generated from README.Rmd. Please edit that file -->
tdncc
=====

<!-- badges: start -->
<!-- badges: end -->
tdncc is a collection of functions used at the Therpeudics Development Network Coordinating Center (TDNCC). This is a group at Seattle Children's Research Institute working on applying statistical methods to cystic fibrosis. This package is currently experimental - use at your own risk! (and if you find bugs let us know)

Installation
------------

tdncc is not currently uploaded to CRAN. To install the development version from [GitHub](https://github.com/) please enter the following commands into an R console:

``` r
# install.packages("devtools")
devtools::install_github("scri-tdncc/tdncc", build_vignettes = T)
```

Usage
-----

To learn about the algorithms for synonym mapping, we recommend starting with the built-in vignettes. These can be opened in RStudio using the command

``` r
browseVignettes("tdncc")
```

There are currently two vignettes:

-   **Master and Synonym Tables** - Master and synonym tables are programming-friendly versions of the [CFTR2](https://www.cftr2.org/) project spreadsheets. This vignette details our automated process of generating them.
-   **Standardizing Variants** - demonstrates how to use a synonym table to standardize variant inputs into legacy coding. Algorithmic details are included also.

For within-TDNCC users the master and synonym tables are available on the shared drive, so we recommend jumping to the second vignette to get going quickly.
